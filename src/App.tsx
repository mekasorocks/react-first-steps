import * as React from "react";
import "./App.css";
import { Person, IPersonProps } from "./Person/Person";

interface IAppProps {}
interface IAppState {
  persons: Array<IPersonProps>,
  showPersons: boolean;
}

class App extends React.Component<IAppProps, IAppState> {


  public static getDerivedStateFromProps(nextProps: IAppProps, prevState: IAppState): IAppProps {
    console.log("getDerivedStateFromProps");
    return prevState;
  }

  public componentDidMount() {
    console.log("componentDidMount");
  }

  readonly deletePersonHandler = (personIndex: number) => {
    const persons = [...this.state.persons];
    persons.splice(personIndex, 1);
    this.setState({
      persons: persons
    } as IAppState)
  }

  togglePersonsHandler = () => {
    const show = this.state.showPersons;
    this.setState({showPersons: !show});
  }

  readonly vornameChangeHandler = (event: React.ChangeEvent<HTMLInputElement>, personId: string) => {
    const personIndex = this.state.persons.findIndex(person => {
      return person.id === personId;
    });
    const person = {
      ...this.state.persons[personIndex]
    }

    person.vorname = event.target.value;
    
    const persons = [...this.state.persons];
    persons[personIndex] = person;

    this.setState({
      persons: persons
    } as IAppState);
  }

  constructor(props: IAppProps, state: IAppState) {
    super(props, state);
    console.log("Konstruktor");
    this.state = {
      persons: [
        { id: 'a', name: "Mustermann", vorname: "Max", alter: 34 } as IPersonProps,
        { id: 'b', name: "Musterfrau", vorname: "Mini", alter: 28} as IPersonProps
      ],
      showPersons: false
    };
  }

  public render() {
    const buttonStyleClasses = [];
    buttonStyleClasses.push('showPersons');
    buttonStyleClasses.push('pointer');

    let persons = null;

    if (this.state.showPersons) {
      persons = (
        <div>
          {
            this.state.persons.map((person, index) => {
              return <Person 
                id={person.id}
                name={person.name} 
                vorname={person.vorname} 
                alter={person.alter} 
                key={person.id} 
                change={this.vornameChangeHandler.bind(this, event, person.id)} />
            })
          }
        </div>
      )
      buttonStyleClasses.splice(0, buttonStyleClasses.length);
      buttonStyleClasses.push('hidePersons');
      buttonStyleClasses.push('pointer');
      buttonStyleClasses.push('bold');
      
    }

    return (
      <div className="App">
        <h1>Ich bin React</h1>
        <button onClick={this.togglePersonsHandler} className={buttonStyleClasses.join(' ')}>Toggle Personen</button>
        {persons}
      </div>
    );
  }  
}

export default App;
