import * as React from "react";

export interface IPersonProps {
  id: string;
  name: string;
  vorname: string;
  alter?: number;
  change: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

export const Person: React.SFC<IPersonProps> = props => {
  return (
    <div>
      <h1>
        Hallo {props.vorname} {props.name}
      </h1>
      <p>Du bist {props.alter} Jahre alt.</p>
      {props.children}
      <input type="text" onChange={props.change} />
    </div>
  );
};
